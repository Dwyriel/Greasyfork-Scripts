// ==UserScript==
// @name         Fextralife Remove Twitch and Fix Layout
// @namespace    https://gitlab.com/Dwyriel
// @version      1.3.1
// @description  Removes twitch stream and fix layout for better readability and usability.
// @author       Dwyriel
// @license      MIT
// @match        https://*.fextralife.com/*
// @grant        none
// @homepageURL  https://gitlab.com/Dwyriel/Greasyfork-Scripts
// @downloadURL  https://gitlab.com/Dwyriel/Greasyfork-Scripts/-/raw/main/FextralifeEnhancements/script.js
// @updateURL    https://gitlab.com/Dwyriel/Greasyfork-Scripts/-/raw/main/FextralifeEnhancements/script.js
// ==/UserScript==

(function () {
    'use strict';
    const minMargin = 16, maxMargin = 73.5 - minMargin, formMinMargin = 8, formMaxMargin = 60 - formMinMargin;
    const snapWidth = 480, desktopWidth = 1200, largeMobile = 768, buggyNavbarBreakpoint = 1218;

    const wrapper = document.getElementById("wrapper");
    const fexMain = document.querySelector(".fex-main");
    const navbar = document.querySelector(".navbar");
    const pageChunk = document.querySelector(".page-chunk");
    const form = document.getElementById("form-header");
    const discussionNew = document.getElementById("discussions-new").parentNode;

    const removeStreamAndSidebar = () => {
        document.getElementById("sidebar-wrapper")?.remove();
        document.getElementById("fextrastream")?.remove();
    };

    //makes the main content look good on bigger screens and remove the giant spacing on the upper part
    const desktopFix = () => {
        fexMain.style = "max-width: 1024px;";
        form.style = "max-height: 60px; margin-top: 0px;";
    };

    //navbar won't show up when width is between 1200 and 1218 otherwise
    const navbarFix = (windowWidth) => {
        navbar.style = windowWidth > desktopWidth && windowWidth < buggyNavbarBreakpoint ? "display: block !important" : "";
    };

    //Bunch of small changes to make it look more consistent
    const smallSpacingFixes = (windowWidth) => {
        wrapper.style.paddingLeft = "0px";
        discussionNew.style.setProperty("padding", "0px", "important");
        let marginSize = "0px";
        if (windowWidth > desktopWidth) {
            marginSize = "auto";
            pageChunk.style.marginLeft = "0px";
            form.style.height = `${formMaxMargin}px`;
        }
        else if (windowWidth >= largeMobile) {
            marginSize = `${((windowWidth - largeMobile) / (desktopWidth - largeMobile) * maxMargin) + minMargin}px`;
            form.style.height = `${((windowWidth - largeMobile) / (desktopWidth - largeMobile) * formMaxMargin) + formMinMargin}px`;
        }
        else if (windowWidth > snapWidth) {
            marginSize = `${minMargin}px`;
            form.style.height = `${formMinMargin}px`;
        }
        fexMain.style.marginLeft = marginSize;
        fexMain.style.marginRight = marginSize;
    };

    //function to be called every 'resize' event
    const fixLayout = () => {
        let windowWidth = window.innerWidth;
        navbarFix(windowWidth);
        smallSpacingFixes(windowWidth);
    };

    removeStreamAndSidebar();
    desktopFix();
    fixLayout();
    window.addEventListener('resize', () => fixLayout());
})();