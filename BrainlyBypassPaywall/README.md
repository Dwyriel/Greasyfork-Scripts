# BrainlyBypassPaywall
Clears brainly's local storage to reset daily count to bypass the daily limit/paywall. Also remove the video ads and closes all popups after a small delay (to keep native functionality).

Sometimes Brainly changes the name of the variable they store in the local storage to keep track of your usage, this script brute forces its way through by completely resetting it rather than just unsetting/changing one specific entry, meaning it will always work until they rework the website.
