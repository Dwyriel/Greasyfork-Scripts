// ==UserScript==
// @name         Brainly Bypass Paywall
// @namespace    https://gitlab.com/Dwyriel
// @version      1.5.4
// @description  Clears brainly's local storage to reset daily count to bypass the daily limit/paywall. Also remove ads and popups.
// @author       Dwyriel
// @license      MIT
// @match        *://*brainly.pl/*
// @match        *://*znanija.com/*
// @match        *://*brainly.lat/*
// @match        *://*brainly.com.br/*
// @match        *://*nosdevoirs.fr/*
// @match        *://*eodev.com/*
// @match        *://*brainly.ro/*
// @match        *://*brainly.co.id/*
// @match        *://*brainly.in/*
// @match        *://*brainly.ph/*
// @match        *://*brainly.com/*
// @grant        none
// @run-at       document-start
// @homepageURL  https://gitlab.com/Dwyriel/Greasyfork-Scripts
// @downloadURL  https://gitlab.com/Dwyriel/Greasyfork-Scripts/-/raw/main/BrainlyBypassPaywall/script.js
// @updateURL    https://gitlab.com/Dwyriel/Greasyfork-Scripts/-/raw/main/BrainlyBypassPaywall/script.js
// ==/UserScript==

(function () {
    'use strict';
    const elementsToRemove = [
        "[data-testid^='ad_below_']" //Annoying video ads
    ];
    const config = { attributes: true, childList: true, subtree: true };
    let loginPopupHidden = false, promoPopupHidden = false;
    let mutationObserver;
    const mutationCallback = () => {
        mutationObserver.disconnect();
        for (let query of elementsToRemove) {
            let nodes = document.querySelectorAll(query);
            for (let node of nodes)
                node.remove();
        }
        document.body.classList.remove("sg-dialog-no-scroll");
        if (!loginPopupHidden) { //Login popup/modal
            let loginPopup = document.querySelector(`[data-testid='registration_promo_modal_module_id']`);
            if (loginPopup) {
                loginPopup.setAttribute("style", 'display: none;');
                loginPopupHidden = true;
            }
        }
        if (!promoPopupHidden) { //Promotion popup/modal
            let promoPopup = document.querySelector(`[data-testid="offer_modal_module_id"]`);
            if (promoPopup) {
                promoPopup.setAttribute("style", 'display: none;');
                promoPopupHidden = true;
            }
        }
        mutationObserver.observe(document.body, config);
    }
    try {
        localStorage.clear();
    } catch (err) {
        console.error("Couldn't clear local storage, the browser is most likely blocking access to it. (are all cookies being blocked?)");
        console.error(err);
    }
    document.addEventListener("DOMContentLoaded", () => {
        mutationObserver = new MutationObserver(mutationCallback);
        mutationCallback();
    });
})();