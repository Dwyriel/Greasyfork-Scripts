# Youtube Cleaner Video Player

Removes any overlay youtube might put on top of the video player that isn't important, like the branding watermark or product ads from the channel, for slightly better video visibility.

You can install this script from [greasyfork](https://greasyfork.org/en/scripts/464709-youtube-cleaner-video-player).